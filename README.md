# LTI PACB Renyi

## Usage

- run 'main.m', which will start monte carlo sampling and computing the bounds, it will save the results to '.mat' file, you will have to stop it manually when you are happy with monte-carlo convergence.
- run 'AiStatsPlotCases.m' to plot multiple saved files at once, note it will wait for files to update and then update the plot, this way you can run this scipt on a seperate instance of matlab and view the results live.
- run 'makePlots.m' to actually generate the plot containing the generalisation gap and the bound.

## Results for default system.
![image info](Renyi.png)

![image info](RenyiCaseswD2.png)