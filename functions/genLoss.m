function L = genLoss(f,fg)
%genLoss computes generalised loss for predictor f
% L=genLoss(f,fg)

    %Compute generalisation loss
    fe=find_fe(f,fg);
    P=dlyap(fe.A,fe.B*fg.Q*fe.B',[],eye(size(fe.A))); % solve P=Ae*P*Ae'+ Ke*Qe*Ke'
    % or in matlab notation solve: Ae*P*Ae'-P+Ke*Q*Ke'=0
    L=fe.C*P*fe.C'+fe.D*fg.Q*fe.D';
end