function [Constants,ConstantsNs,constant_labels,constantNs_labels,f_end,fs_prior] = sampleConstants_from_prior(Nf,Ns,y,w,f0,fg,log_prior_fcn,proprnd_fcn,propStep,MH_thin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
N_threads=10;
%Nf/N_threads>100 => N_threads<Nf/100
N_threads=max(1,min(N_threads,floor(Nf/100)));

constantNs_labels={'$\hat{\mathcal{L}}_N(f_{\pi,i}'};
constant_labels={   '\mathcal{L}_N(f_{\pi,i}',...
                    'G_{e,\pi,i}',...
                    'G_{e1,\pi,i}',...
                    '\bar{G}_{f1,\pi,i}',...
                    '\bar{G}_{f2,\pi,i}'};
ConstantsNs=cell(0,1);
Constants=cell(0,1);

[f0.Gf,f0.M,f0.g,f0.barGf1,f0.barGf2]=Gf_new(f0);
%         Ge_priorss{q},Ge1_priorss{q},barGf1_priorss{q},barGf2_priorss{q}

%         quants=@(f) [normL1(find_fe(f,fg));normL1_alternative(find_fe(f,fg));f.barGf1;f.barGf2];
        quants={@(f)normL1(find_fe(f,fg));@(f)normL1_alternative(find_fe(f,fg));@(f)f.barGf1;@(f)f.barGf2};
        [f_end,Q,acc,rej,fs_prior]=PAC.customMH_parfor(f0,Nf,log_prior_fcn,@(f) proprnd_fcn(f,propStep),MH_thin,N_threads,quants,0,0);
        fprintf('acceptance rate: %f%%\n',100*acc/(acc+rej));
    
    %   [fs_prior{q},~]=customMH(f0s{q},Nf,@log_prior,@proprnd,MH_thin);


        ConstantsNs{1}= empLossNsBulk(fs_prior,y,w,Ns); %L_hats
%         for i=1:numel(fs_prior)
%             ConstantsNs{1}(:,i)=empLossNs(fs_prior{i},y,w,Ns)
%         end


        Constants{1}=cellParFun(@(f) genLoss(f,fg),fs_prior,N_threads); %L

        Constants{2}=Q(1,:);%(cellfcn(@(f) normL1(find_fe(f,fg)),fs_prior{q}));  
        Constants{3}=Q(2,:);
        Constants{4}=Q(3,:);
        Constants{5}=Q(4,:);
%         Constants{6}=nan(3,numel(fs_prior));
%         a=nan(1,numel(fs_prior));
%         b=nan(1,numel(fs_prior));
%         c=nan(1,numel(fs_prior));
%         for i=1:numel(fs_prior)
%             [a(i),b(i),c(i)] = PAC.compNL_Gs(fs_prior{i},fg);
%         end
%         Constants{6}=[a;b;c];
end