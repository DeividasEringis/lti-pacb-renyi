function f = proprnd(f0,step)
%   Generate a proposal
% step=0.35^2;

f=f0;
% f.A(1,1)=f.A(1,1)+step*randn;
% f.A(1,2)=f.A(1,2)+step*randn;

f.A=f.A+triu(0.1*step*randn(size(f.A))/sqrt(numel(f.A)));
f.B=f.B+step*randn(size(f.B))/sqrt(numel(f.B));
f.C=f.C+step*randn(size(f.C))/sqrt(numel(f.C));
if size(f.D,2)==1
    f.D=f.D+step*randn(size(f.D))/sqrt(numel(f.D));
else
    f.D(1,2)=f.D(1,2)+step*randn;
end



f.lpos=nan;
f.lprior=nan;


end

