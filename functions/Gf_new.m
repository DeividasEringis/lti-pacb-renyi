function [Gf_new,M,g,barGf1,barGf2] = Gf_new(varargin)
%Gf_new computes G_f for bounded case as stated in the paper
%   Uses:
%  [Gf] = Gf(f),
%            f=struct, with f.A,f.B,f.C,f.D defined
%  [Gf] = Gf(f,\hat{M},\hat{\gamma}),
%             f=struct, with f.A,f.B,f.C,f.D defined
%             \hat{M},\hat{\gamma} are scalars s.t.
%             |f.A^k|<=\hat{M}*\hat{\gamma}^k
% Optionally: 
%   [Gf,M,g] = Gf(f),
%   also returns M=\hat{M},g=\hat{\gamma}, s.t. 
%   M,g = argmin M/(1-g), and  norm(f.A^k,2)<=M*g^k, for k<=20
%   while bound for k<=20 is guaranteed, constrained are tightened to
%   ensure it holds for higher k, but it is not guaranteed.


f=varargin{1};
if nargin==1
    [M,g]=findMgammaOpt(f.A);
else
    M=varargin{2};
    g=varargin{3};
end
MCB=M*norm(f.C,2)*norm(f.B,2);
Gf_new=(MCB/((1-g)^(3/2)))*(1+norm(f.D,2)+MCB/(1-g));
barGf1=MCB/(1-g);
barGf2=(1+norm(f.D,2)+MCB/(1-g))/(1-g);
end