function f_big = appendZerosToSS(f,big_n)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

n=size(f.A,1);
f_big=f;
if big_n>n
f_big.A=[f_big.A,zeros(n,big_n-n);zeros(big_n-n,big_n)];
f_big.B=[f_big.B;zeros(big_n-n,size(f_big.B,2))];
f_big.C=[f_big.C,zeros(size(f_big.C,1),big_n-n)];
else
f_big.A=f_big.A(1:big_n,1:big_n);
f_big.B=f_big.B(1:big_n,:);
f_big.C=f_big.C(:,1:big_n);
end
end