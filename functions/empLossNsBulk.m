function L_hat_Ns_ = empLossNsBulk(fs,y,w,Ns)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here

gpuDev=gpuDevice();
reset(gpuDev);

tic

    Nmax=1e5;
    Nmax=min(Nmax,size(w,2)); %no point in computing for more than 1e6 points
    % numerical value will be close to identical.


L_hat_Ns=gpuArray(zeros(numel(Ns),numel(fs)));
p=size(y,1);
q=size(w,1);

sysMat=cellfun(@(f) [f.A,f.B;f.C,f.D],fs,'UniformOutput',false);

n=size(fs{1}.A,1);
sA=spalloc(n*numel(fs),n*numel(fs),numel(fs)*n^2);
for i=1:numel(fs)
    idxr=(i-1)*(n)+1:i*(n);
    idxc=(i-1)*(n)+1:i*(n);
    sA(idxr,idxc)=fs{i}.A;
end
sA=gpuArray(sparse(sA));

sC=spalloc(p*numel(fs),n*numel(fs),numel(fs)*p*n);
for i=1:numel(fs)
    idxr=(i-1)*(p)+1:i*(p);
    idxc=(i-1)*(n)+1:i*(n);
    sC(idxr,idxc)=fs{i}.C;
end
sC=gpuArray(sparse(sC));


sB=zeros(n*numel(fs),q);
for i=1:numel(fs)
    idxr=(i-1)*(n)+1:i*(n);
    sB(idxr,:)=fs{i}.B;
end
sB=gpuArray(sB);
w=w(:,1:min(max(Ns),end));
y=y(:,1:min(max(Ns),end));
y=gpuArray(y);
w=gpuArray(w);
device=gpuDevice();
AM=device.MaxGridSize(1);
MemPerDouble=8;
%#fs*n*w_subset*MemPerDouble<AM/2; => w_subset<AM/(2*#fs*n*MemPerDouble)
w_subset=min(floor(AM/(numel(fs)*(n+p)*MemPerDouble)),size(w,2));
w_subset=max(w_subset,1);
Bw=sB*w(:,1:w_subset); %of size #fs*n*w_subset
% Bw=sB*w;

sD=zeros(p*numel(fs),q);
for i=1:numel(fs)
    idxr=(i-1)*(p)+1:i*(p);
    sD(idxr,:)=fs{i}.D;
end
sD=gpuArray(sD);
Dw=sD*w(:,1:w_subset);
% Dw=sD*w;


X=gpuArray(zeros(numel(fs)*(n),1));

% Y(k)=sC*X(k)+Dw(k)
% Y(k)=sC*sA*X(k-1)+sC*Bw(k-1)+Dw(k)
% Y(k)=sC*sA^2*X(k-2)+sC*sA*Bw(k-2)+sC*Bw(k-1)+Dw(k)
% Y(k)=sC*sA^step*X(k-step)+ \sum_j=1^step  sC*sA^{j-1}*Bw(k-j) + Dw(k)
% X=A*X+Bw

L_hats=gpuArray(zeros(numel(fs),1));
start_idx=gpuArray(1);
% y=gpuArray(y);
% w=gpuArray(w);
% progressbar('empLossNsBulk')
Ns=gpuArray(Ns);
Nmax=gpuArray(Nmax);
for s=1:numel(Ns)
%     progressbar(Ns(s)/min(Nmax,Ns(end)))
%     fprintf('N@%i',start_idx)
    N_=min(Ns(s),Nmax);
    for i=start_idx:N_
%         y_hat=f.C*x+f.D*w(:,i);
%         x=f.A*x+f.B*w(:,i);

        idx=mod(i-1,w_subset)+1;
        y_hat=sC*X+Dw(:,idx);
        X=sA*X+Bw(:,idx);

        z=y(:,i)-y_hat;
%         for j=1:numel(fs)
%             temp_idx=(j-1)*(p)+1:(j)*(p);
%             L_hats(j)=L_hats(j)+gather(dot(z(temp_idx),z(temp_idx)));
%         end
        L_hats=L_hats+z.^2;
        if idx==w_subset
            Bw=sB*w(:,min((i+1),end):min((i+w_subset),end));
            Dw=sD*w(:,min((i+1),end):min((i+w_subset),end));
        end
    end
    L_hat_Ns(s,:)=(L_hats./N_);
    start_idx=N_+1;
%     fprintf(' -> %i , (%5.2f%%)\n',start_idx-1,100*(start_idx-1)/Ns(end))
end
L_hat_Ns_=gather(L_hat_Ns);

toc



end

%Old version, about 3x slower:


% % % tic
% % % 
% % %     Nmax=1e5;
% % %     Nmax=min(Nmax,size(w,2)); %no point in computing for more than 1e6 points
% % %     % numerical value will be close to identical.
% % % 
% % % L_hat_Ns=gpuArray(zeros(numel(Ns),numel(fs)));
% % % p=size(y,1);
% % % q=size(w,1);
% % % 
% % % sysMat=cellfun(@(f) [f.A,f.B;f.C,f.D],fs,'UniformOutput',false);
% % % n=size(fs{1}.A,1);
% % % s=(n+q)*(n+p);
% % % sM=spalloc(size(sysMat{1},1)*numel(sysMat),size(sysMat{1},2)*numel(sysMat),numel(sysMat)*s);
% % % for i=1:numel(sysMat)
% % %     idxr=(i-1)*(n+p)+1:i*(n+p);
% % %     idxc=(i-1)*(n+q)+1:i*(n+q);
% % %     sM(idxr,idxc)=sysMat{i};
% % % end
% % % sM=gpuArray(sparse(sM));
% % % % states0=cell2mat(cellfun(@(mat) ([zeros(n,1);w(:,1)]),sysMat,'UniformOutput',false)');
% % % 
% % % % states=(states0);
% % % in=gpuArray(zeros(numel(fs)*(n+q),1));
% % % out=gpuArray(zeros(numel(fs)*(n+p),1));
% % % % 
% % % % 
% % % % ys=nan(numel(fs),1);
% % % idx_y=spalloc(1,numel(out),p*numel(fs));%3:3:numel(states);
% % % idx_i=0;
% % % while idx_i<numel(out)
% % %     idx_i=idx_i+n;
% % %     idx_y(idx_i+1:idx_i+p)=1;
% % %     idx_i=idx_i+p;
% % % end
% % % idx_y=gpuArray(full(idx_y==1));
% % % idx_out_states=not(idx_y);
% % % 
% % % idx_w=spalloc(q,numel(in),q*numel(fs));%3:3:numel(states);
% % % idx_i=0;
% % % while idx_i<numel(in)
% % %     idx_i=idx_i+n;
% % %     for i=1:q
% % %         idx_w(i,idx_i+i)=1;
% % %     end
% % %     idx_i=idx_i+q;
% % % end
% % % idx_w=gpuArray(full(idx_w==1));
% % % if q>1
% % % idx_in_states=not(any(idx_w));
% % % else
% % %   idx_in_states=not(idx_w);
% % % end
% % % % output_idx=spalloc(1,p*numel(fs),p*numel(fs));%3:3:numel(states);
% % % % idx_i=0;
% % % % while idx_i<numel(states)
% % % %     idx_i=idx_i+n;
% % % %     idx(idx_i+1:idx_i+p)=1;
% % % %     idx_i=idx_i+p;
% % % % end
% % % 
% % % 
% % % 
% % % L_hats=gpuArray(zeros(numel(fs),1));
% % % start_idx=gpuArray(1);
% % % y=gpuArray(y);
% % % w=gpuArray(w);
% % % % progressbar('empLossNsBulk')
% % % Ns=gpuArray(Ns);
% % % Nmax=gpuArray(Nmax);
% % % for s=1:numel(Ns)
% % % %     progressbar(Ns(s)/min(Nmax,Ns(end)))
% % %     fprintf('N@%i',start_idx)
% % %     N_=min(Ns(s),Nmax);
% % %     for i=start_idx:N_
% % % %         y_hat=f.C*x+f.D*w(:,i);
% % % %         x=f.A*x+f.B*w(:,i);
% % %         in(idx_in_states)=out(idx_out_states);
% % %         for j=1:q
% % %             in(idx_w(j,:))=w(j,i);
% % %         end
% % %         out=(sM*in);
% % %         z=y(:,i)-out(idx_y);
% % % %         for j=1:numel(fs)
% % % %             temp_idx=(j-1)*(p)+1:(j)*(p);
% % % %             L_hats(j)=L_hats(j)+gather(dot(z(temp_idx),z(temp_idx)));
% % % %         end
% % %         L_hats=L_hats+z.^2;
% % %     end
% % %     L_hat_Ns(s,:)=(L_hats./N_);
% % %     start_idx=N_+1;
% % %     fprintf(' -> %i , (%5.2f%%)\n',start_idx-1,100*(start_idx-1)/Ns(end))
% % % end
% % % L_hat_Ns_=gather(L_hat_Ns);
% % % 
% % % % 
% % % % 
% % % % for t=1:size(ys,2)
% % % %     states=sM*states;
% % % %     ys(:,t)=states(idx);
% % % %     states(idx)=w(:,t+1);
% % % % 
% % % %     z=y(:,t)-ys(:,t);
% % % % 
% % % % %     for i=3:3:numel(states)
% % % % %         ys(i/3,t)=states(i);
% % % % %         states(i)=w(:,t+1);
% % % % %     end
% % % % end
% % % % % step1=AsGpu*vec
% % % % e=y(:,1:Ns)-ys;
% % % % e=e.^2;
% % % % e=mean(e,2);
% % % % EmpLoss_gpu=e';
% % % toc







