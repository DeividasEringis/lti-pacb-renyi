function n = normL1_alternative(ss)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
tol=1e-7;
lim=1e5;

n=norm(ss.D,2);
new_n=n+2*norm(ss.C*ss.B,2); %new_n=|D|+|CA^0B|
old_AkB=ss.B;
k=1;
while (abs(new_n-n)>tol && k<=lim) || k<50
    n=new_n;
    old_AkB=ss.A*old_AkB;
    k=k+1;
    new_n=n+(k+1)*norm(ss.C*old_AkB,2);
end
n=new_n;
end

