function fe = find_fe(f,fg)
%find_fe Compute the error system
%   computes the error system from predictor and data generator in
%   innovation form.
n=fg.n;
p=fg.p;
C1=fg.C(1:p,:);C2=fg.C(p+1:end,:);
if (size(f.B,2)==1)
    fe.A=[fg.A,zeros(size(fg.A,1),size(f.A,2)); f.B*C2, f.A];
    fe.B=[fg.B;[zeros(size(f.B,1),p), f.B]];
    fe.C=[C1-f.D*C2, -f.C];
    fe.D=[eye(p,p),-f.D];
else
% D1=fg.D(1:p,:);D2=fg.D(p+1:end,:);
    fe.A=[fg.A,zeros(size(fg.A,1),size(f.A,2)); f.B*fg.C, f.A];
    fe.B=[fg.B;f.B];
    fe.C=[C1-f.D*fg.C, -f.C];
    fe.D=[1,0]-f.D;

end
% fe.A=[fg.A,zeros(size(fg.A,1),size(f.A,2)); f.B*C2, f.A];
% fe.B=[fg.B;[f.B]];
% fe.C=[C1-f.D*C2, -f.C];
% fe.D=[[1,0]-f.D];
end

