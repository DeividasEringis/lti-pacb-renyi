
files ={'pac_const.mat','pac_logn.mat','pac_sqrtn.mat'};
titles={'$\lambda=1$','$\lambda=ln(N+1)$','$\lambda=\sqrt{N}$'};


lastUpdated={0,0,0,0};
callbacks={};
pre='E_rho |L-L_hat| and Bounds and exp(Renyi)';
fig=figure(7);
clf
tiledlayout(1,numel(files),"TileSpacing","tight")
for file_i=1:numel(files)
    ax=nexttile; %subplot(1,numel(files),file_i);
    callbacks{file_i}=@() updateAxes(ax,files{file_i},titles{file_i});
end


while true
    fig.Name=sprintf('%s, %s',pre,datetime(now,'ConvertFrom','datenum'));
    
    for i=1:numel(files)
        try
            if isempty(dir(files{i}))==false
                dn=datenum(dir(files{i}).date);
                if dn>lastUpdated{i}
                
                callbacks{i}();
                lastUpdated{i}=dn;
    
                end
            end
        catch ME
            disp(ME)
        end
    end

    
    axes=fig.findobj('Type','Axes');
    miny=inf; maxy=-inf;
    for i=1:numel(axes)
        if axes(i).YLim(1)<miny
            miny=axes(i).YLim(1);
        end
        if axes(i).YLim(2)>maxy
            maxy=axes(i).YLim(2);
        end
    end
    for i=1:numel(axes)
        axes(i).YLim=[miny,maxy];
    end
    
    miny=inf; maxy=-inf;
    for i=1:numel(axes)
        yyaxis(axes(i),'right')
        if axes(i).YLim(1)<miny
            miny=axes(i).YLim(1);
        end
        if axes(i).YLim(2)>maxy
            maxy=axes(i).YLim(2);
        end
    end
    for i=1:numel(axes)
        axes(i).YLim=[miny,maxy];
        yyaxis(axes(i),'left')
    end
    drawnow
    fig.Units="inches";
    linewidth=3;
    aspect=4/7;
    fig.Position(3:4)=[numel(files)*linewidth,linewidth/aspect];
    exportgraphics(fig,'RenyiCaseswD2.png')
    pause(5)
end%while
% % title('$\rho(f)\propto exp(-\lambda_N\hat{\mathcal{L}}_N(f))$, $\lambda_N=\sqrt{N}$','Interpreter','latex')

% 
% savefig('RenyiCaseswD2.fig')
% exportgraphics(fig,'RenyiCaseswD2.eps')
% matlab2tikz('RenyiCaseswD2.tex')


function updateAxes(ax,filename,axTitle)
    cla(ax)
    load(filename) %
    pac_n=numel(pac);
    
%     left_color=[0,0,0];
    right_color=[0,1,0];
    
    for i=1:pac_n
    if pac{i}.Bounds(1)>0
    % hold on
    a=loglog(ax,pac{i}.Ns,pac{i}.absL_L_hat,'-r');
    % a.DisplayName=sprintf('$\\omega_%i:\\;E_{f\\sim\\rho}|\\mathcal{L}_N(f)-\\hat{\\mathcal{L}}_N(f)|$',i);
    a.DisplayName=sprintf('$E_{f\\sim\\rho}|\\mathcal{L}(f)-\\hat{\\mathcal{L}}_N(f)|$');
    
    hold(ax,"on")
    b=loglog(ax,pac{i}.Ns,pac{i}.Bounds,'-b');
    % b.Color=a.Color;
    % b.DisplayName=sprintf('$\\omega_%i:\\;r_N^R$',i);
    b.DisplayName=sprintf('$r_N$');
    % hold off
    yyaxis(ax,"right")
    set(ax,'YColor',right_color)
    set(ax,'YScale','log')
    % hold on
    c=loglog(ax,pac{i}.Ns,sqrt(pac{i}.BoundConsts.eRenyi_arg)./pac{i}.BoundConsts.Z_hat_inv_,'-g');
    c.DisplayName=sprintf('$\\bar{D}_2(\\rho|\\pi)$');
    hold(ax,"on")
    % hold off
    yyaxis(ax,"left")
    end
    
    end
    % pl_E_rho_L_hats.Parent.Title.Interpreter="latex";
    legend([a,b,c],'Interpreter','latex','Location','northeast');
    grid(ax,"on")
%     hold off
    xticks(ax,10.^(0:5))
%     title(ax,sprintf('%s, \n%s',axTitle,dir(filename).date))
    title(ax,axTitle,'Interpreter','latex')
end



