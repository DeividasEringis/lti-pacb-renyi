function [lp,g,f] = log_prior(f)
%log_prior log-likelihood of prior distribution on hypothesis class

%  a=-0.5; b=0.5;
% if f.A(1,1)<a || f.A(1,1)>b || max(abs(eig(f.A)))>1-eps
%     lp=-inf;
% else
%     lp=log(1/(b-a));
% end
[f.Gf,f.M,f.g,f.barGf1,f.barGf2]=Gf_new(f);


if max(abs(eig(f.A)))>0.99 %|| f.Gf>10  %|| f.Gf>2
    lp=-inf;
%     M=nan;
%     g=nan;
else
%     [M,g]=findMgammaOpt(f.A);
%     M=nan;
%     g=nan;
    %prior = exp(-M/(1-g));
    %l_prior=-M/(1-g)
%     lp=0;%-M/(1-g);
%     lp=-50*(f.Gf-1)^2;
   
    lp=-1*f.Gf; % p(f)=exp(-f.Gf^2)
    pvec=[reshape(f.A,[],1);reshape(f.B,[],1);reshape(f.C,[],1);reshape(f.D,[],1)];
    mu=zeros(numel(pvec),1);
    Sigma=0.02*eye(numel(pvec));
    lpdf=log(mvnpdf(pvec,mu,Sigma));
    lp=lpdf;
end


g=0;
f.lprior=lp;
% f.M=M;
% f.g=g;
end

